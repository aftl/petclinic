FROM openjdk:latest
# Using openjdk as the Docker Image

COPY . /spring-petclinic
# Copying the Source Code in the container

WORKDIR /spring-petclinic/
# Changing the Working Directory 

RUN  if [[ ! -d "build/libs" ]];  then ./gradlew build ; fi
# Building the project using gradlew

EXPOSE 8080
# Exposing the 8080 port for running the Web Application


CMD ["java","-jar","build/libs/spring-petclinic-2.6.1.jar"]
# Running the Application
